//
//  PaypalDetailsViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class PaypalDetailsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var payPalEmailIdTextField: MDCTextField!
    
    
    //MARK: - Variables
    var paypalEmailController: MDCTextInputControllerOutlined?
    var window : UIWindow?
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    
    //MARK: - Setup View
    func setupView() {
        paypalEmailController = MDCTextInputControllerOutlined(textInput: payPalEmailIdTextField)
        paypalEmailController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        paypalEmailController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        paypalEmailController?.borderRadius = 10.0

    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpTapped(_ sender: Any) {
        
        if payPalEmailIdTextField.isValidEmail(payPalEmailIdTextField.text ?? "") {
            apiRequestObject["paypal_email"] = payPalEmailIdTextField.text ?? ""
            apiRequestObject["payment_method"] = "paypal"
            createUser(user: apiRequestObject)
        }
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        createUser(user: apiRequestObject)
    }
    
    @IBAction func loginButton(_ sender: Any) {
        
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    
    //MARK: - Private Methods
}


//MARK: - APICalls
extension PaypalDetailsViewController {
    
    private func createUser (user: [String: String]) {
        Utility.showLoading()
        
        UserData.createUser(user: user) { (data, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                Utility.setDonorTabAsRootViewController()
                
            } else {
                self.showAlert(title: "", message: "Could not be able create user.")
            }
        }
    }
}

