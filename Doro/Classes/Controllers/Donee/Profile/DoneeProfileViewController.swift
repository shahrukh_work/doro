//
//  DoneeProfileViewController.swift
//  Doro
//
//  Created by a on 05/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import SDWebImage
import KeychainSwift

class DoneeProfileViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var deleteCredentialView: UIView!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var occupationLabel: UILabel!
    
    //MARK: - Variables
    var window : UIWindow?
    var doneeNotificationVC = DoneeNotificationsViewController()
    var doneeProfileSettingVC = DoneeProfileSettingViewController()
    var doneePayoutSettingVC = DoneePayoutRequestViewController()
    var doneePaypalSettingVC = DoneePayPalSettingViewController()
    var loginVC = LoginViewController()
    let keychain = KeychainSwift()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataSource()
        deleteCredentialView.isHidden = true
        if let credentails = keychain.get("credentails") {
            let cred = credentails.components(separatedBy: ",")
            
            if cred[0] == DataManager.shared.getUser()?.user.email {
                deleteCredentialView.isHidden = false

            }
        }
    }
    
    //MARK: - setupView
    func setupView () {
        self.navigationController?.navigationBar.isHidden = true
        userProfileImageView.layer.cornerRadius = userProfileImageView.frame.width / 2
    }
    
    
    //MARK: - IBActions
    @IBAction func deletePressed(_ sender: Any) {
        showDeleteConfirmation ()
    }
    
    @IBAction func notificationTapped(_ sender: Any) {
        doneeNotificationVC = DoneeNotificationsViewController(nibName: "DoneeNotificationsViewController", bundle: nil)
        self.navigationController?.pushViewController(doneeNotificationVC, animated: true)
    }
    
    @IBAction func profileSettingTapped(_ sender: Any) {
        doneeProfileSettingVC = DoneeProfileSettingViewController(nibName: "DoneeProfileSettingViewController", bundle: nil)
        self.navigationController?.pushViewController(doneeProfileSettingVC, animated: true)
    }
    
    @IBAction func payoutRequestTapped(_ sender: Any) {
        doneePayoutSettingVC = DoneePayoutRequestViewController(nibName: "DoneePayoutRequestViewController", bundle: nil)
        self.navigationController?.pushViewController(doneePayoutSettingVC, animated: true)
    }
    
    @IBAction func paypalSettingTapped(_ sender: Any) {
        doneePaypalSettingVC = DoneePayPalSettingViewController(nibName: "DoneePayPalSettingViewController", bundle: nil)
        self.navigationController?.pushViewController(doneePaypalSettingVC, animated: true)
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        DataManager.shared.deleteUser()
        Utility.loginRootViewController()
    }
    
    
    //MARK: Datasouce
    func dataSource() {
        
        Utility.showLoading()
        UserProfileData.getUserProfile {[weak self] (result, error, status) in
                
            if error == nil {
                self?.nameLabel.text = result?.fullName
                self?.occupationLabel.text = result?.occupation
                let imageUrl = result?.imageUrl
                let trimmedImageUrl = imageUrl?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
                
                if let url = URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + trimmedImageUrl!  ) {
                    self?.userProfileImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
                }
                
            } else {
                self?.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    func showDeleteConfirmation () {
        
        if let _ = keychain.get("credentials") {
            let alert = UIAlertController(title: "Delete FaceID/TouchID", message: "Please confirm to delete local authentication.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {_ in
                self.keychain.delete("credentials")
                DataManager.shared.deleteFinger()
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            self.present(alert, animated: true, completion:nil)
            
        } else {
            self.showAlert(title: "", message: "You did not configure FaceID/TouchID.")
        }
    }
}
