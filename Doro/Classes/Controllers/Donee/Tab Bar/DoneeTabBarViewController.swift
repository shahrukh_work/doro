//
//  DoneeTabBarViewController.swift
//  Doro
//
//  Created by a on 08/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class DoneeTabBarViewController: UITabBarController {
    
    //MARK:- Variables
    var walletNavController = UINavigationController()
    var scanQRNavController = UINavigationController()
    var profileNavController = UINavigationController()
        
            
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.selectedIndex = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: - View Setup
    func setupView() {
        let walletVC = DoneeWalletViewController()
        walletVC.tabBarItem.image = UIImage(named: "wallet")!.withRenderingMode(.alwaysOriginal)
        walletVC.tabBarItem.selectedImage = UIImage(named: "wallet-selected")!.withRenderingMode(.alwaysOriginal)
        walletVC.tabBarItem.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0);  // sets UItabBarItem position according to given constants
        walletNavController = UINavigationController(rootViewController: walletVC)
                
        let homeVC = DoneeHomeViewController()
        homeVC.tabBarItem.image = UIImage(named: "Group 3668")!.withRenderingMode(.alwaysOriginal)
        homeVC.tabBarItem.imageInsets = UIEdgeInsets(top: -18, left: 0, bottom: 18, right: 0);    // sets UItabBarItem position according to given constants
        scanQRNavController = UINavigationController(rootViewController: homeVC)
                
        let profileVC1 = DoneeProfileViewController()
        profileVC1.tabBarItem.image = UIImage(named: "profile")!.withRenderingMode(.alwaysOriginal)
        profileVC1.tabBarItem.selectedImage = UIImage(named: "profile-selected")!.withRenderingMode(.alwaysOriginal)
        profileVC1.tabBarItem.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0);    // sets UItabBarItem position according to given constants
        profileNavController = UINavigationController(rootViewController: profileVC1)
          
        let tabBarList = [walletNavController, scanQRNavController, profileNavController]
        self.viewControllers = tabBarList
        
        //--------Adds a button to tabbar---------
        /*
        let button = UIButton()
        button.setImage(UIImage(named: "Group 3668"), for: .normal)
        button.sizeToFit()
        button.translatesAutoresizingMaskIntoConstraints = false

        tabBar.addSubview(button)
        tabBar.centerXAnchor.constraint(equalTo: button.centerXAnchor).isActive = true
        tabBar.topAnchor.constraint(equalTo: button.centerYAnchor).isActive = true
        */
        //-----------------------------------------
    }
}
