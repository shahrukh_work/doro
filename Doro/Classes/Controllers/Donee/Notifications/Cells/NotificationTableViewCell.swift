//
//  NotificationTableViewCell.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    
    @IBOutlet weak var notificationImage: UIImageView!
    @IBOutlet weak var notificationDescriptionlabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var ellipseImage: UIImageView!
    
    
    //MARK: - Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        notificationImage.cornerRadius = notificationImage.frame.width / 2
    }
    
    func configure (notification: DoneeNotificationData) {
        notificationImage.sd_setImage(with: URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + notification.imageURL.trimmingCharacters(in: .whitespacesAndNewlines))! , placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
        notificationDescriptionlabel.text =  notification.message

        if !notification.received {
            notificationDescriptionlabel.text =  notification.message + " $\(notification.amount)"

        }
        timeLabel.text = Utility.dataInEnglish(notification.createdAt)
    }
}
