//
//  DoneeHomeViewController.swift
//  Doro
//
//  Created by a on 09/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import Photos

class DoneeHomeViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var downloadImageView: UIImageView!
    @IBOutlet weak var printImageView: UIImageView!
    @IBOutlet weak var shareImageView: UIImageView!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var printButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    
    //MARK: - Variables
    var isQrScnner = false
    var generatedImage: UIImage?
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageView.alpha = 1.0
        downloadImageView.isHidden = true
        printImageView.isHidden = true
        shareImageView.isHidden = true
        downloadButton.isHidden = true
        printButton.isHidden = true
        shareButton.isHidden = true
    }
    
    
    //MARK: - View Setup
    func setupView() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        let path = "https://doro.codesorbit.com/home/\(DataManager.shared.getUser()?.user.email ?? "")/\(DataManager.shared.getUser()?.user.id ?? -1)"
        generatedImage = generateQRCode(from: path)
        imageView.image =  generatedImage
    }
    
    
    //MARK: - Actions
    @IBAction func notificationTapped(_ sender: Any) {
        let notificationVC = DoneeNotificationsViewController(nibName: "DoneeNotificationsViewController", bundle: nil)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        
        guard let validQR = generatedImage else { return }
        let data = validQR.jpegData(compressionQuality: 1.0)
        let image = UIImage(data: data!)
        let activityItem: [UIImage] = [image!]
        let activity = UIActivityViewController(activityItems: activityItem as [UIImage], applicationActivities: [])
        activity.popoverPresentationController?.sourceView  = self.view
        self.present(activity, animated: true, completion: nil)
    }
    
    
       //MARK: - Add image to Library
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        
        print(image)
        
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    //MARK: - Selectors
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        isQrScnner.toggle()
        
        if isQrScnner{
            imageView.alpha = 0.5
            downloadImageView.isHidden = false
            printImageView.isHidden = false
            shareImageView.isHidden = false
            downloadButton.isHidden = false
            printButton.isHidden = false
            shareButton.isHidden = false

        } else {
            imageView.alpha = 1.0
            downloadImageView.isHidden = true
            printImageView.isHidden = true
            shareImageView.isHidden = true
            downloadButton.isHidden = true
            printButton.isHidden = true
            shareButton.isHidden = true
        }
        // Your action
    }
  
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.utf8)
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            guard let colorFilter = CIFilter(name: "CIFalseColor") else { return nil }

            filter.setValue(data, forKey: "inputMessage")

            filter.setValue("H", forKey: "inputCorrectionLevel")
            colorFilter.setValue(filter.outputImage, forKey: "inputImage")
            colorFilter.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1") // Background white
            colorFilter.setValue(CIColor(color: #colorLiteral(red: 0, green: 0.5378677845, blue: 0.7376958132, alpha: 1)), forKey: "inputColor0") // Foreground or the barcode RED
            guard let qrCodeImage = colorFilter.outputImage
                else {
                    return nil
            }
            let scaleX = imageView.frame.size.width / qrCodeImage.extent.size.width
            let scaleY = imageView.frame.size.height / qrCodeImage.extent.size.height
            let transform = CGAffineTransform(scaleX: scaleX, y: scaleY)


            if let output = colorFilter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
}
