//
//  DoneePayoutRequestViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class DoneePayoutRequestViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var totalBalanceLabel: UILabel!
    @IBOutlet weak var withDrawAmountTextField: UITextField!
    @IBOutlet weak var withDrawOption1Button: UIButton!
    @IBOutlet weak var withDrawOption2Button: UIButton!
    @IBOutlet weak var withDrawOption3Button: UIButton!
    @IBOutlet weak var withDrawOption4Button: UIButton!
    @IBOutlet weak var totalBalanceView: UIView!
    
    
    //MARK: - Variables
    var amount : Int = 0
    var withDrawAmountController: MDCTextInputControllerOutlined?

    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getReceivedAmount()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        withDrawAmountTextField.delegate = self
        withDrawAmountTextField.setLeftPaddingPoints(8)
        totalBalanceView.cornerRadius = 10.0
        withDrawAmountTextField.delegate = self
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func paymentSelectionTapped(_ sender: UIButton) {
        // Create a list of all tags
        let allButtonTags = [1, 2, 3, 4]
        let currentButtonTag = sender.tag
        withDrawAmountTextField.text = ""
        switch currentButtonTag {
            case 1:
            amount = 2
            
            case 2:
            amount = 5
            
            case 3:
            amount = 10
            
        default:
            amount = 20
        }
        
        allButtonTags.filter { $0 != currentButtonTag }.forEach { tag in
            if let button = self.view.viewWithTag(tag) as? UIButton {
                // Deselect/Disable these buttons
                button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                
            }
        }
        // Select/Enable clicked button
    
        sender.layer.borderColor = #colorLiteral(red: 0.1843137255, green: 0.6823529412, blue: 0.9764705882, alpha: 0.7194991438)
    }
    
    @IBAction func withDrawTapped(_ sender: Any) {
        
        if amount == 0 && (withDrawAmountTextField.text == ""){
            withDrawAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withDrawOption1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.withDrawAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withDrawOption1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            
        } else if (withDrawAmountTextField.text! == "$") {
            withDrawAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
           
           DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.withDrawAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
           }
            
        }
        else if withDrawAmountTextField.text != "" || withDrawAmountTextField.text != "" {
            
            var amount = withDrawAmountTextField.text
            let _ = amount?.removeFirst()
            withdrawFromDonee(donee: DataManager.shared.getUser()?.user.id ?? -1, amount: Double(amount ?? "0")!)
            
        } else if amount != 0 {
            withdrawFromDonee(donee: DataManager.shared.getUser()?.user.id ?? -1, amount: Double(amount))
        }
    }
    
    
    //MARK: - Private Methods
    private func withdrawFromDonee (donee: Int, amount: Double) {
        
        DoneeWidthDrawal.setDoneeWidthdrawal(doneeId: donee, amount: amount) { (result, error, status) in
            
            if error == nil {
                self.showSuccessViewController ()
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    private func showSuccessViewController () {
        
        let popup = DonorSuccessAlertViewController()
        popup.modalPresentationStyle = .overCurrentContext
        popup.modalTransitionStyle = .crossDissolve
        let transition = CATransition()
        transition.duration = 1.5
        view.window!.layer.add(transition, forKey: kCATransition)
        present(popup, animated: true, completion: {
            self.withDrawAmountTextField.text = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.dismiss(animated: true, completion: nil)
            }
        })
        
    }
}


//MARK:- Textfield Delegates
extension DoneePayoutRequestViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == withDrawAmountTextField {
        amount = 0
        withDrawOption1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
        withDrawOption2Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
        withDrawOption3Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
        withDrawOption4Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
        textField.layer.borderColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        textField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
        if textField.text?.count == 0{
            let attributedString = NSMutableAttributedString(string: "$")
            withDrawAmountTextField.attributedText = attributedString
        }
        
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    private func getReceivedAmount () {
        
        DoneeAmount.getDoneeReceivedAndWithdrawnAmount {[weak self] (result, error, status) in
            
            if error == nil {
                self?.totalBalanceLabel.text = "$\(result?.data.first?.total ?? 0)"
                
            } else {
                self?.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
}
