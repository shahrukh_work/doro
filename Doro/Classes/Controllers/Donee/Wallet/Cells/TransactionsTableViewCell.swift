//
//  TransactionsTableViewCell.swift
//  Doro
//
//  Created by a on 05/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper

class TransactionsTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var amountLabel2: UILabel!
    @IBOutlet weak var transactionLabel: UILabel!
    
    
    //MARK: - Variables
    var dataSource = Mapper<Transaction>().map(JSON: [:])!
    
    
    //MARK: - lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        amountLabel2.isHidden = true
        transactionLabel.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: - Public Methods
    func config(data: Transaction, indexPath: IndexPath) {
        self.dataSource = data
        descriptionLabel.text = data.message
        dateLabel.text = Utility.dataInEnglish(data.createdAt)
        amountLabel.text = String(data.amount)
        descriptionLabel.textColor = #colorLiteral(red: 0.368627451, green: 0.368627451, blue: 0.368627451, alpha: 1)
        if !data.received {
            descriptionLabel.textColor = #colorLiteral(red: 0, green: 0.5952172494, blue: 0.154437752, alpha: 1)
            amountLabel.text = "-$\(data.amount)"
        }
    }
}
