//
//  DoneeWalletViewController.swift
//  Doro
//
//  Created by a on 05/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper

class DoneeWalletViewController: UIViewController {

    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var receivedAmountLabel: UILabel!
    @IBOutlet weak var withdrawnAmountLabel: UILabel!
    @IBOutlet weak var amountStackView: UIStackView!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var noResultView: UIView!
    
    
    //MARK: - variables
    var doneeNotificationVC = DoneeNotificationsViewController()
    var listData : [Transaction] = []
    var offset = 0
    
    
    //MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataSource()
    }
    
    //MARK: - setupView
    func setupView() {
        self.navigationController?.navigationBar.isHidden = true
      //  tableView.rowHeight = UITableView.automaticDimension
       // tableView.estimatedRowHeight = 60
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    //MARK: - IBActions
    @IBAction func notificationTapped(_ sender: Any) {
        doneeNotificationVC = DoneeNotificationsViewController(nibName: "DoneeNotificationsViewController", bundle: nil)
        self.navigationController?.pushViewController(doneeNotificationVC, animated: true)
    }
    
    
    //MARK: - Data Source
    func dataSource() {
        getTransactions()
        getReceivedAmount ()
    }
    
    
    //MARK: - Private methods
    private func getTransactions (offset: Int = 0) {
        
        if offset == 0 {
            self.listData.removeAll()
        }
        
        Transactions.getTransactions(offset: offset) { (data, error, status) in
            
            if data != nil {
                self.listData.append(contentsOf: data?.data ?? [])
                self.tableView.reloadData()
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    private func getReceivedAmount () {
        
        DoneeAmount.getDoneeReceivedAndWithdrawnAmount {[weak self] (result, error, status) in
            
            if error == nil {
                self?.receivedAmountLabel.text = "$\(result?.data.first?.recievedAmount ?? 0)"
                self?.withdrawnAmountLabel.text = "$\(result?.data.first?.withdrawnAmount ?? 0)"
                
            } else {
                self?.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - Tableview Datasource & Delegates
extension DoneeWalletViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
       // return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.register(TransactionsTableViewCell.self, indexPath: indexPath)
        cell.config(data: listData[indexPath.row], indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            offset += 1
            getTransactions(offset: offset)
        }
    }
}
