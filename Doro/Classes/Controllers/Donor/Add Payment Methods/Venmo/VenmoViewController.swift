//
//  VenmoViewController.swift
//  Doro
//
//  Created by a on 08/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class VenmoViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var venmoEmailIDTextField: MDCTextField!
    @IBOutlet weak var confirmVenmoEmailIDTextField: MDCTextField!
    
    //MARK: - Variables
    var venmoEmailIdController: MDCTextInputControllerOutlined?
    var confirmVenmoEmailIdController: MDCTextInputControllerOutlined?
    
    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        venmoEmailIdController = MDCTextInputControllerOutlined(textInput: venmoEmailIDTextField)

        confirmVenmoEmailIdController = MDCTextInputControllerOutlined(textInput: confirmVenmoEmailIDTextField)
        venmoEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        venmoEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
 
        confirmVenmoEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        confirmVenmoEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        venmoEmailIdController?.borderRadius = 10.0
        confirmVenmoEmailIdController?.borderRadius = 10.0
        
        self.venmoEmailIDTextField.tag = 0
        self.confirmVenmoEmailIDTextField.tag = 1
        self.venmoEmailIDTextField.delegate = self
        self.confirmVenmoEmailIDTextField.delegate = self
    
    }
 
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)

    }
    @IBAction func doneTapped(_ sender: Any) {
        createStripePayment()
    }
    
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    func createStripePayment () {
    
        if !venmoEmailIDTextField.isValidEmail(venmoEmailIDTextField.text!) {
            self.showAlert(title: "", message: "Please enter a valid email addess")
            return
        }
        
        if !confirmVenmoEmailIDTextField.isValidEmail(confirmVenmoEmailIDTextField.text!) {
            self.showAlert(title: "", message: "Please enter a valid email addess")
            return
        }
        
        if venmoEmailIDTextField.text == confirmVenmoEmailIDTextField.text {
            createStripeAccount ()
            
        } else {
            self.showAlert(title: "", message: "Confirm stripe email does not match above.")
        }
    }

    private func createStripeAccount () {
        
        UserData.updatePaymentEmail(email: venmoEmailIDTextField.text ?? "", paymentMethod: "stripe") { (result, error, status) in
            
            if error == nil {
                
                if UserData.shared.doneeIdForWithdrawal != -1 {
                    let creditCardVC = DonorTransferAmountViewController()
                    self.navigationController?.pushViewController(creditCardVC, animated: true)
                    
                } else {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                
            } else {
                self.showAlert(title: "", message: "Stripe Account not created!")
            }
        }
    }
}
