//
//  DonorTransactionTableViewCell.swift
//  Doro
//
//  Created by Macbook on 21/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

protocol RetryDelegate {
    func register ()
}
class DonorTransactionTableViewCell: UITableViewCell {

    //MARK: - IBOutlets
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var transactionLabel: UILabel!
    
    var delegate: RetryDelegate?
    
    //MARK: - lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    //MARK: - Configure
    func configure (data: Transaction, indexPath: IndexPath) {
        descriptionLabel.text = data.message
        dateLabel.text = Utility.changeDateFormate(dataInString: data.createdAt)
        amountLabel.text = "-$\(data.amount)"
    }
    
    @IBAction func retryTapped(_ sender: Any) {
        //print("tapped")
        retryButtonPressed = true
        delegate?.register()
    }
    
}
