//
//  DonorWalletViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class DonorWalletViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var transferedAmountLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var transferView: UIView!
    @IBOutlet weak var amountStackView: UIStackView!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var noResultView: UIView!
    
    
    //MARK: - Variables
    var donorNotificationVC = DonorNotificationsViewController()
    var listData : [Transaction] = []
    var offset = 0
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.listData.removeAll()
        setupView()
        dataSource()
    }
    
    
    //MARK: - Setup View
    func setupView() {
        self.navigationController?.navigationBar.isHidden = true
        tableView.rowHeight = 60
        tableView.delegate = self
        tableView.dataSource = self
        noResultView.isHidden = true
    }
    
    
    //MARK: - IBActions
    @IBAction func notificationTapped(_ sender: Any) {
        donorNotificationVC = DonorNotificationsViewController(nibName: "DonorNotificationsViewController", bundle: nil)
        self.navigationController?.pushViewController(donorNotificationVC, animated: true)
    }
    
    
    //MARK: - Data Source
    func dataSource() {
        getTransactions()
        getTransfermount()
    }
    
    private func getTransactions (offset: Int = 0) {
        
        Transactions.getTransactions(userType: .donor, offset: offset) { (result, error, status) in
            self.listData.append(contentsOf: result?.data ?? [])
            
            if self.listData.count > 0 {
                self.amountStackView.isHidden = false
                self.transactionLabel.isHidden = false
                self.tableView.isHidden = false
                self.noResultView.isHidden = true
                self.tableView.reloadData()
                
            } else {
                self.amountStackView.isHidden = true
                self.transactionLabel.isHidden = true
                self.tableView.isHidden = true
                self.noResultView.isHidden = false
            }
        }
    }
    
    private func getTransfermount () {
        
        DonorAmount.getDonorReceivedAndWithdrawnAmount {[weak self] (result, error, status) in
            
            if error == nil {
                self?.transferedAmountLabel.text = "$\(result?.data.first?.totalTransfer ?? 0)"
                
            } else {
                self?.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK: - Tableview Datasource & Delegates
extension DonorWalletViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.register(DonorTransactionTableViewCell.self, indexPath: indexPath)
        cell.configure(data: self.listData[indexPath.row], indexPath: indexPath )
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            offset += 1
            getTransactions(offset: offset)
        }
    }
}


//MARK: - RetryDelegate
extension DonorWalletViewController: RetryDelegate {
    func register() {
        let donorNotificationVC = SelectPaymentViewController(nibName: "SelectPaymentViewController", bundle: nil)
        self.navigationController?.pushViewController(donorNotificationVC, animated: true)
        
    }
}
