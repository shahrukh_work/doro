//
//  DonorHomeViewController.swift
//  Doro
//
//  Created by a on 09/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class DonorHomeViewController: UIViewController {


    //MARK: - IBOutlets
    var obj = SignupPaymentViewController()
    var qrCodeString = ""
    @IBOutlet weak var scannerView: QRScannerView! {
        didSet {
            scannerView.delegate = self
        }
    }
  
    
    //MARK: - Variables
    var qrData: QRData?


    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        checkPendingStatus ()
        scannerView.startScanning()
    }
       
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        scannerView.stopScanning()
    }
    
    
    //MARK: - View Setup
    func setupView() {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func checkPendingStatus () {
        
        CheckPending.loadData { (result, error, status) in
            
            if error == nil {
                
                if result?.data.count ?? 0 > 0 {
                    let user = DataManager.shared.getUser()
                    user?.user.userStatus = result?.data.first?.status ?? ""
                    DataManager.shared.setUser(user: user?.toJSONString() ?? "")
                    UserData.shared.priceForGuest = result?.data.first?.pendingAmount ?? 0.0
                    UserData.shared.doneeIdForWithdrawal = result?.data.first?.doneeID ?? -1
                    UserData.shared.user.userStatus = result?.data.first?.status ?? ""
                    self.showControllerOnConditions()
                }
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
    
    //MARK: - Actions
    @IBAction func notificationTapped(_ sender: Any) {
        let notificationVC = DonorNotificationsViewController(nibName: "DonorNotificationsViewController", bundle: nil)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    
    //MARK: - private method
    func showTransferScreen () {
        let transferVC2 = DonorTransferAmountViewController()
        transferVC2.doneeId = self.qrCodeString
        self.navigationController?.pushViewController(transferVC2, animated: true)
    }
    
    func showControllerOnConditions () {
        let transferVC = SelectPaymentViewController()
        let user = DataManager.shared.getUser()?.user
        
        if user?.stripeEmail == "undefined" && user?.paypalEmail == "undefined"  {
            self.navigationController?.pushViewController(transferVC, animated: true)
            
        } else if user?.stripeEmail == "undefined" || user?.paypalEmail != "undefined" && user?.defaultPaymentGateway == "paypal" {
            showTransferScreen()
            
        } else if user?.stripeEmail != "undefined" && user?.defaultPaymentGateway == "stripe" || user?.paypalEmail == "undefined" {
            showTransferScreen ()
            
        } else if user?.paypalEmail != "undefined" && user?.stripeEmail != "undefined" && user?.defaultPaymentGateway == "stripe" {
            showTransferScreen ()
            
        } else if user?.paypalEmail != "undefined" && user?.stripeEmail != "undefined" && user?.defaultPaymentGateway == "paypal" {
            showTransferScreen ()
        }
    }
}


//MARK: - QRScanner View Delegate
extension DonorHomeViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        //let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        //scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        //presentAlert(withTitle: "Error", message: "Scanning Failed. Please try again")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        qrCodeString = str!
        self.qrData = QRData(codeString: str)
        let urlArray = qrCodeString.components(separatedBy: "/")
        
        if urlArray.count > 0 {
            qrCodeString = urlArray[urlArray.count - 1]
            
            if let number = Int(qrCodeString) {
                UserData.shared.doneeIdForWithdrawal = number
                
            } else {
                //scannerView.stopScanning()
                self.showAlert(title: "", message: "QR-Code is not correct")
                scannerView.startScanning()
                return
            }
        }
        scannerView.stopScanning()
        
        UserProfileData.getUserProfile(doneeId: Int(qrCodeString)!) { [self] (result, error, status) in
            
            if error == nil {
                self.showControllerOnConditions()
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
                self.scannerView.startScanning()
            }
        }
    }
}
