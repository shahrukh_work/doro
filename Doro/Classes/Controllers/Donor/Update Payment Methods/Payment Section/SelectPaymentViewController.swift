//
//  SelectPaymentViewController.swift
//  Doro
//
//  Created by a on 08/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class SelectPaymentViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var selection1: UIImageView!
    @IBOutlet weak var selection2: UIImageView!
    @IBOutlet weak var selection3: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    
    //MARK: - Variables
    var paymentMethod : String = ""
    var isChangePaymentGateway = false
    
    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupView()
        selection2.image = #imageLiteral(resourceName: "unselected")
        selection1.image = #imageLiteral(resourceName: "unselected")
        paymentMethod = DataManager.shared.getUser()?.user.defaultPaymentGateway ?? ""
        if DataManager.shared.getUser()?.user.defaultPaymentGateway == "paypal" {
            selection1.image = #imageLiteral(resourceName: "selected")
            
        } else if DataManager.shared.getUser()?.user.defaultPaymentGateway == "stripe" {
            selection2.image = #imageLiteral(resourceName: "selected")
        }
        
        if isChangePaymentGateway {
            nextButton.setTitle("Done", for: .normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func paymentOptionTapped(_ sender: UIButton) {
        
        if sender.tag == 0 {
            paymentMethod = "paypal"
            selection1.image = #imageLiteral(resourceName: "selected")
            selection2.image = #imageLiteral(resourceName: "unselected")
            
        } else if sender.tag == 1 {
            paymentMethod = "stripe"
            selection1.image = #imageLiteral(resourceName: "unselected")
            selection2.image = #imageLiteral(resourceName: "selected")
            
        } else {
            paymentMethod = "creditcard"
            selection1.image = #imageLiteral(resourceName: "unselected")
            selection2.image = #imageLiteral(resourceName: "unselected")
        }
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        if isChangePaymentGateway {
            
            if UserData.shared.user.stripeEmail == "undefined" {
                self.showAlert(title: "", message: "You do not have stripe account! Please add stripe account from payment method.")
                return
                
            } else if UserData.shared.user.paypalEmail == "undefined" {
                self.showAlert(title: "", message: "You do not have paypal account! Please add paypal account from payment method.")
                return
            }
            changePaymentGateway(name: paymentMethod)
            return
        }
        
        if paymentMethod == "paypal" {
            
            if DataManager.shared.getUser()?.user.paypalEmail == "undefined" {
               let payPalVC = PaypalViewController()
               self.navigationController?.pushViewController(payPalVC, animated: true)
                
            } else {
                let payPalVC = PaypalPaymentViewController()
                self.navigationController?.pushViewController(payPalVC, animated: true)
            }
            
        } else if paymentMethod == "stripe" {
            
            if DataManager.shared.getUser()?.user.stripeEmail == "undefined" {
               let payPalVC = VenmoViewController()
               self.navigationController?.pushViewController(payPalVC, animated: true)
                
            } else {
                let payPalVC = VenmoPaymentViewController()
                self.navigationController?.pushViewController(payPalVC, animated: true)
            }
        }
    }
    
    
    //MARK: - Private Method
    func changePaymentGateway (name: String) {
        Utility.showLoading()
        APIClient.shared.defaultPaymentGatewayMethod(paymentMethod: name) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if status == 404 {
                    self.showAlert(title: "", message: "Error setting default payment gateway.")
                    return
                }
                let user = DataManager.shared.getUser()
                user?.user.defaultPaymentGateway = name
                UserData.shared.user.defaultPaymentGateway = name
                DataManager.shared.setUser(user: (user?.toJSONString())!)
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        }
    }
}
