//
//  PaypalPaymentViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class PaypalPaymentViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var paypalEmailIdTextField: MDCTextField!
    @IBOutlet weak var newPaypalEmailIdTextField: MDCTextField!
    @IBOutlet weak var confirmPayPalEmailIdTextField: MDCTextField!
    
    
    //MARK: - Variables
    var paypalEmailIdController: MDCTextInputControllerOutlined?
    var newPaypalEmailIdController: MDCTextInputControllerOutlined?
    var confirmPaypalEmailIdController: MDCTextInputControllerOutlined?
    
    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        paypalEmailIdController = MDCTextInputControllerOutlined(textInput: paypalEmailIdTextField)
        newPaypalEmailIdController = MDCTextInputControllerOutlined(textInput: newPaypalEmailIdTextField)
        confirmPaypalEmailIdController = MDCTextInputControllerOutlined(textInput: confirmPayPalEmailIdTextField)
        paypalEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        paypalEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        newPaypalEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        newPaypalEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        confirmPaypalEmailIdController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        confirmPaypalEmailIdController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        paypalEmailIdController?.borderRadius = 10.0
        newPaypalEmailIdController?.borderRadius = 10.0
        confirmPaypalEmailIdController?.borderRadius = 10.0
        
        self.paypalEmailIdTextField.tag = 0
        self.newPaypalEmailIdTextField.tag = 1
        self.confirmPayPalEmailIdTextField.tag = 2
        self.paypalEmailIdTextField.delegate = self
        self.newPaypalEmailIdTextField.delegate = self
        self.confirmPayPalEmailIdTextField.delegate = self
    
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        
        if paypalEmailIdTextField.isValidEmail(paypalEmailIdTextField.text!) &&
            newPaypalEmailIdTextField.isValidEmail(newPaypalEmailIdTextField.text!) &&
            confirmPayPalEmailIdTextField.isValidEmail(confirmPayPalEmailIdTextField.text!) {
            
            updatePayPalEmail()
            
        } else {
            showAlert(title: "", message: "Invalid email address")
        }
    }
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
    
    
    //MARK: - Private Methods
    private func updatePayPalEmail () {
        Utility.showLoading()
        UpdatePaypalEmail.updatePaypalEmail(oldPaypalEmail: paypalEmailIdTextField.text!, newPaypalEmail: newPaypalEmailIdTextField.text!, confirmNewPaypalEmail: confirmPayPalEmailIdTextField.text!, { (result, error, status) in
            
            if error == nil {
                self.showAlert(title: "", message: result ?? "")
                
            } else {
                self.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
            
        })
    }
}
