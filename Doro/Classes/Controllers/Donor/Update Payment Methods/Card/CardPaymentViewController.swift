//
//  CardPaymentViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import MaterialComponents

class CardPaymentViewController: UIViewController, UITextFieldDelegate {

    //MARK: - IBOutlets
    @IBOutlet weak var nameTextField: MDCTextField!
    @IBOutlet weak var cardNoTextField: MDCTextField!
    @IBOutlet weak var cVVNoTextField: MDCTextField!
    @IBOutlet weak var validThroughTextField: MDCTextField!
    
    
    //MARK: - Variables
    var nameController: MDCTextInputControllerOutlined?
    var cardNoController: MDCTextInputControllerOutlined?
    var cVVNoController: MDCTextInputControllerOutlined?
    var validThroughController: MDCTextInputControllerOutlined?
    let datePicker = UIDatePicker()
    
    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: - Setup View
    func setupView() {
        nameController = MDCTextInputControllerOutlined(textInput: nameTextField)
        cardNoController = MDCTextInputControllerOutlined(textInput: cardNoTextField)
        cVVNoController = MDCTextInputControllerOutlined(textInput: cVVNoTextField)
        validThroughController = MDCTextInputControllerOutlined(textInput: validThroughTextField)
        nameController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        nameController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        cardNoController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        cardNoController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        cVVNoController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        cVVNoController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        validThroughController?.activeColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        validThroughController?.floatingPlaceholderActiveColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.6545911815)
        showDatePicker()
        nameController?.borderRadius = 10.0
        cardNoController?.borderRadius = 10.0
        cVVNoController?.borderRadius = 10.0
        validThroughController?.borderRadius = 10.0
        
        self.nameTextField.tag = 0
        self.cardNoTextField.tag = 1
        self.cVVNoTextField.tag = 2
        self.validThroughTextField.tag = 3
        self.nameTextField.delegate = self
        self.cardNoTextField.delegate = self
        self.cVVNoTextField.delegate = self
        self.validThroughTextField.delegate = self
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func doneTapped(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DonorProfileViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func updateTapped(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DonorProfileViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        validThroughTextField.inputAccessoryView = toolbar
        validThroughTextField.inputView = datePicker
        }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/yyyy"
        validThroughTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1

        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }

}
