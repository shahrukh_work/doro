//
//  DonorNotificationsViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper

class DonorNotificationsViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- Variables
    var offset = 0
    var notifications = Mapper<DonorNotifications>().map(JSON: [:])!
    
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        dataSource ()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK:- Setup View
    func setupView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func dataSource () {
        getNotifications()
    }
    
    
    //MARK:- IBActions
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: - Private Methods
    private func getNotifications (pageNo: Int = 0) {
        Utility.showLoading()
        DonorNotifications.donorNotifications(offset: pageNo) {[weak self] (result, error, status) in
            
            if error == nil {
                self?.notifications = result!
                self?.tableView.reloadData()
            } else {
                
            }
        }
    }
}


//MARK: - Tableview Datasource & Delegates
extension DonorNotificationsViewController: UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.register(DonorNotificationTableViewCell.self, indexPath: indexPath)
        cell.configure(notification:notifications.data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            offset += 1
            getNotifications(pageNo: offset)
        }
    }
}
