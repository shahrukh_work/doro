//
//  DonorTabBarViewController.swift
//  Doro
//
//  Created by a on 08/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

class DonorTabBarViewController: UITabBarController {
    
    //MARK:- Variables
    var walletNavController = UINavigationController()
    var scanQRNavController = UINavigationController()
    var profileNavController = UINavigationController()
    var tabBarList = [UIViewController]()
            
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.selectedIndex = 1
    }
    
    
    //MARK: - View Setup
    func setupView() {
        let walletVC = DonorWalletViewController()
        walletVC.tabBarItem.image = UIImage(named: "wallet")!.withRenderingMode(.alwaysOriginal)
        walletVC.tabBarItem.selectedImage = UIImage(named: "wallet-selected")!.withRenderingMode(.alwaysOriginal)
        walletVC.tabBarItem.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0);  // sets UItabBarItem position according to given constants
        walletNavController = UINavigationController(rootViewController: walletVC)
                
        let homeVC = DonorHomeViewController()
        homeVC.tabBarItem.image = UIImage(named: "Group 3668")!.withRenderingMode(.alwaysOriginal)
        homeVC.tabBarItem.imageInsets = UIEdgeInsets(top: -18, left: 0, bottom: 18, right: 0);    // sets UItabBarItem position according to given constants

        scanQRNavController = UINavigationController(rootViewController: homeVC)
                
        let profileVC1 = DonorProfileViewController()
        profileVC1.tabBarItem.image = UIImage(named: "profile")!.withRenderingMode(.alwaysOriginal)
        profileVC1.tabBarItem.selectedImage = UIImage(named: "profile-selected")!.withRenderingMode(.alwaysOriginal)
        profileVC1.tabBarItem.imageInsets = UIEdgeInsets(top: 8, left: 0, bottom: -8, right: 0);    // sets UItabBarItem position according to given constants
        profileNavController = UINavigationController(rootViewController: profileVC1)
          
        tabBarList = [walletNavController, scanQRNavController, profileNavController]
        self.viewControllers = tabBarList
    }
}
