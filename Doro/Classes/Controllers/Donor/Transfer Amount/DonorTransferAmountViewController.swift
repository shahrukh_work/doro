//
//  DonorTransferAmountViewController.swift
//  Doro
//
//  Created by a on 06/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import Stripe
import ObjectMapper
import Braintree

class DonorTransferAmountViewController: UIViewController {
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var enterPasswordLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var transferButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var transferButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordTextfieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var selectCardButton: UIButton!
    @IBOutlet weak var stripePasswordLabel: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var transferAmountTextField: UITextField!
    @IBOutlet weak var stripePayButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var withdrawOpt1Button: UIButton!
    @IBOutlet weak var withdrawOpt2Button: UIButton!
    @IBOutlet weak var withdrawOpt3Button: UIButton!
    @IBOutlet weak var withdrawOpt4Button: UIButton!
    @IBOutlet weak var enterPasswordLabel: UILabel!
    @IBOutlet weak var enterPasswordTextField: UITextField!
    
    //MARK: - Variables
    var amount : Double = 0
    var doneeId = "0"
    var customerContext: STPCustomerContext?
    var paymentContext: STPPaymentContext?
    var hasStripeId = DataManager.shared.getUser()?.user.stripeCustId != ""
    var brainTreeClient: BTAPIClient?
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        doneeId = "\(UserData.shared.doneeIdForWithdrawal)"
        brainTreeClient = BTAPIClient(authorization: "sandbox_8hsqyx5w_9hpqs9mpwn4g9yrg")
        
        if DataManager.shared.getUser()?.user.stripeEmail != "undefined" && DataManager.shared.getUser()?.user.defaultPaymentGateway == "stripe" {
            customerContext = STPCustomerContext(keyProvider: MyAPIClient())
            self.paymentContext = STPPaymentContext(customerContext: customerContext!)
            self.paymentContext?.delegate = self
            self.paymentContext?.hostViewController = self
            self.paymentContext?.paymentAmount = 5000 // This is in cents, i.e. $50 USD
        }       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        dataSource()
        
        if UserData.shared.user.userStatus == "INACTIVE" {
            transferAmountTextField.text = "$\(UserData.shared.priceForGuest)"
            transferAmountTextField.isUserInteractionEnabled = false
            transferAmountTextField.alpha = 0.6
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    //MARK: -
    func brainTreeSetup () {
        Utility.showLoading()
        let paypalDriver = BTPayPalDriver(apiClient: brainTreeClient!)
        paypalDriver.viewControllerPresentingDelegate = self
        paypalDriver.appSwitchDelegate =  self
        let payPalRequest = BTPayPalRequest(amount: "\(amount)")
        
        paypalDriver.requestOneTimePayment(payPalRequest) { [self] (tokenizedPayPalAccount, error) -> Void in
            print("working \(tokenizedPayPalAccount) ")
            print("not working \(error?.localizedDescription ?? "")")
            let amount = Int(self.amount)
            MyAPIClient.sharedClient.updatePaymentToServer(donor: UserData.shared.user.id, donee: Int(self.doneeId)!, amount: "\(amount)", viewController: self)
            
        }
    }
    
    func setupView() {
        transferAmountTextField.delegate = self
        passwordTextField.delegate = self
        transferAmountTextField.setLeftPaddingPoints(8)
        passwordTextField.setLeftPaddingPoints(8)
        userProfileImage.cornerRadius = userProfileImage.frame.width / 2
        self.transferAmountTextField.tag = 0
        self.passwordTextField.tag = 1
        self.transferAmountTextField.delegate = self
        self.passwordTextField.delegate = self
        
        if DataManager.shared.getUser()?.user.stripeEmail != "undefined" && DataManager.shared.getUser()?.user.defaultPaymentGateway == "stripe" {
            enterPasswordLabelTopConstraint.constant = 0
            stripePasswordLabel.isHidden = true
            transferButtonTopConstraint.constant = 0
            passwordTextField.isHidden = true
            passwordTextfieldHeightConstraint.constant = 0
            selectCardButton.setTitle("Select Method", for: .normal)
            
        } else {
            enterPasswordLabelTopConstraint.constant = 40
            transferButtonTopConstraint.constant = 40
            stripePasswordLabel.isHidden = false
            passwordTextField.isHidden = false
            passwordTextfieldHeightConstraint.constant = 40
            stripePayButton.isHidden = true
        }
    }
    
    
    //MARK: - IBActions
    @IBAction func backTapped(_ sender: Any) {
        
        if backButtonTapped{
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: DonorHomeViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            backButtonTapped = false
            
        } else if backOnRetryTapped{
            
            for controller in self.navigationController!.viewControllers as Array {
                
                if controller.isKind(of: DonorWalletViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
            backOnRetryTapped = false
            
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func amountOptionTapped(_ sender: UIButton) {
        // Create a list of all tags
        //TODO: - uncomment for guest user
        if UserData.shared.user.userStatus == "INACTIVE" {
            self.showAlert(title: "", message: "You are forced to pay pending payment first.")
            return
        }
        
        let allButtonTags = [1, 2, 3, 4]
        let currentButtonTag = sender.tag
        transferAmountTextField.text = ""
        switch currentButtonTag {
        case 1:
            amount = 2
            
        case 2:
            amount = 5
            
        case 3:
            amount = 10
            
        default:
            amount = 20
        }
        
        allButtonTags.filter { $0 != currentButtonTag }.forEach { tag in
            if let button = self.view.viewWithTag(tag) as? UIButton {
                // Deselect/Disable these buttons
                button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                
            }
        }
        // Select/Enable clicked button
        sender.layer.borderColor = #colorLiteral(red: 0.1843137255, green: 0.6823529412, blue: 0.9764705882, alpha: 0.7194991438)
    }
    
    @IBAction func payPressed(_ sender: Any) {
        
        if (transferAmountTextField.text! != "") {
            var amt = transferAmountTextField.text!
            amt.remove(at: amt.startIndex)
            
            if !amt.isEmpty {
                amount = Double(amt)!
                
                if amount == 0 {
                    self.showAlert(title: "", message: "You can not transfer zero amount.")
                    return
                }
                
            } else {
                self.showAlert(title: "", message: "Please enter some amount.")
                return
            }
            
        }
        validationForStripe()
    }
    
    @IBAction func transferTapped(_ sender: Any) {
        
        if DataManager.shared.getUser()?.user.stripeEmail != "undefined" && UserData.shared.user.defaultPaymentGateway == "stripe" {
            self.paymentContext?.presentPaymentOptionsViewController()
            
        } else {
            
            if (transferAmountTextField.text! != "") && (passwordTextField.text! != ""){
                var amt = transferAmountTextField.text!
                amt.remove(at: amt.startIndex)
                
                if !amt.isEmpty {
                    amount = Double(amt)!
                    
                    if amount == 0 {
                        self.showAlert(title: "", message: "You can not transfer zero amount.")
                        return
                    }
                    
                } else {
                    self.showAlert(title: "", message: "Please enter some amount.")
                    return
                }
            }
            validationForPaypal()
        }
    }
    
    
    //MARK: Datasouce
    func dataSource() {
        
        Utility.showLoading()
        UserProfileData.getUserProfile ({[weak self] (result, error, status) in
            
            if error == nil {
                self?.nameLabel.text = result?.fullName
                self?.emailLabel.text = result?.email
                let imageUrl = result?.imageUrl
                let trimmedImageUrl = imageUrl?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
                
                if let url = URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + trimmedImageUrl!  ) {
                    self?.userProfileImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
                }
                
            } else {
                self?.showAlert(title: "", message: error?.localizedDescription ?? "")
            }
        })
    }
    
    
    //MARK : - Private Method
    
    func validationForPaypal () {
        
        if transferAmountTextField.text! == "" && passwordTextField.text! == "" && amount == 0 {
            //checking textfield amount password
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            passwordTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.passwordTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if !(transferAmountTextField.text! == "") && (passwordTextField.text! == ""){
            //checking only password
            passwordTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.passwordTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if amount == 0 && (passwordTextField.text! == "") {
            //checking amount variable and password textfield
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if amount == 0 && !(passwordTextField.text! == "") && (transferAmountTextField.text! == ""){
            //checking password filled but amount zero
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if amount != 0 && (passwordTextField.text! == "") {
            //amount has something but password empty
            passwordTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.passwordTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if (transferAmountTextField.text! == "$") && !(passwordTextField.text! == ""){
            //checking only dollar sign available
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if (transferAmountTextField.text! == "$0") || (transferAmountTextField.text! == "0$") && !(passwordTextField.text! == ""){
            //checking password not empty transfer amount
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
           
           DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
           }
            
        } else {
            print(amount)
            //payToAdmin()
            brainTreeSetup()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
        }
        //pay ()
    }
    
    func validationForStripe () {
        
        if transferAmountTextField.text! == "" && amount == 0 {
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if amount == 0 {
            //checking amount variable and password textfield
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if amount == 0  && (transferAmountTextField.text! == ""){
            //checking password filled but amount zero
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
                self.withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if (transferAmountTextField.text! == "$") {
            //checking only dollar sign available
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
            
        } else if (transferAmountTextField.text! == "$0") || (transferAmountTextField.text! == "0$") {
            //checking password not empty transfer amount
            transferAmountTextField.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 0.504682149)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.transferAmountTextField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            }
            return
        }
        self.paymentContext?.requestPayment()
    }
    
    private func pay () {
        self.payToAdmin()
    }
    
    private func updateUserStatus () {
        APIClient.shared.updateStatus(email: UserData.shared.user.email) { (result, error, status) in
            
            if error == nil {
                let user = DataManager.shared.getUser()
                user?.user.userStatus = "ACTIVE"
                DataManager.shared.setUser(user: user?.toJSONString() ?? "")
                UserData.shared.user = user?.user ?? Mapper<User>().map(JSON: [:])!
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popToRootViewController(animated: true)
                
            } else {
                self.showAlert(title: "", message: "You status did not changed.")
            }
        }
    }
    
    func showSuccesVC () {
        let popup = SuccessAlertViewController()
        popup.modalPresentationStyle = .overCurrentContext
        popup.modalTransitionStyle = .crossDissolve
        let transition = CATransition()
        transition.duration = 1
        view.window!.layer.add(transition, forKey: kCATransition)
        present(popup, animated: true, completion: {
            
            self.transferAmountTextField.text = ""
            self.passwordTextField.text = ""            
        })
        //after transition hide current popup after 2 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            
            if  UserData.shared.user.userStatus == "INACTIVE" {
                self.updateUserStatus ()
                
            } else {
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    private func payToAdmin () {
       
        DonorToAdmin.paymentDonorToAdmin(donorId: DataManager.shared.getUser()?.user.id ?? -1, doneeId: self.doneeId, amount: amount, password: passwordTextField.text!) { (result, error, status) in
            
            if error == nil {
                self.showSuccesVC()
                
            } else {
                self.showAlert(title: "Error", message: error?.localizedDescription ?? "")
            }
        }
    }
}


//MARK:- Textfield Delegates
extension DonorTransferAmountViewController : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == transferAmountTextField  {
            amount = 0
            withdrawOpt1Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            withdrawOpt2Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            withdrawOpt3Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            withdrawOpt4Button.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
            textField.layer.borderColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        }
        else if textField == passwordTextField {
            textField.layer.borderColor = #colorLiteral(red: 0.01176470588, green: 0.6549019608, blue: 0.8862745098, alpha: 0.8734749572)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 0.5)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength : Int
        
        if textField == transferAmountTextField && textField.text?.count == 0{
            let attributedString = NSMutableAttributedString(string: "$")
            transferAmountTextField.attributedText = attributedString
        }
        
        if textField == transferAmountTextField {
            maxLength = 10
            
        } else {
            maxLength = 50
        }
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    private func tagBasedTextField(_ textField: UITextField) {
        let nextTextFieldTag = textField.tag + 1
        
        if let nextTextField = textField.superview?.viewWithTag(nextTextFieldTag) as? UITextField {
            nextTextField.becomeFirstResponder()
            
        } else {
            textField.resignFirstResponder()
        }
    }
    
    //Move to next textfield
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.tagBasedTextField(textField)
        return true
    }
}


//MARK: -
extension DonorTransferAmountViewController: STPPaymentContextDelegate, STPAuthenticationContext {
    
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        
    }

    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPPaymentStatusBlock) {
        MyAPIClient.sharedClient.completeCharge(doneeId: Int(doneeId)!, donorId: UserData.shared.user.id, paymentResult, amount: "\(Int(amount)*100)", viewController: self, nil, shippingMethod: nil, completion: completion)
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
    }
}


extension DonorTransferAmountViewController: BTViewControllerPresentingDelegate, BTAppSwitchDelegate {
    // MARK: - BTViewControllerPresentingDelegate

    func paymentDriver(_ driver: Any, requestsPresentationOf viewController: UIViewController) {
        present(viewController, animated: true, completion: nil)
    }

    func paymentDriver(_ driver: Any, requestsDismissalOf viewController: UIViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

    // MARK: - BTAppSwitchDelegate


    // Optional - display and hide loading indicator UI
    func appSwitcherWillPerformAppSwitch(_ appSwitcher: Any) {
        showLoadingUI()

      //  NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingUI), name: NSNotification.Name.UIApplication.didBecomeActiveNotification, object: nil)
    }

    func appSwitcherWillProcessPaymentInfo(_ appSwitcher: Any) {
        hideLoadingUI()
    }

    func appSwitcher(_ appSwitcher: Any, didPerformSwitchTo target: BTAppSwitchTarget) {

    }

    // MARK: - Private methods

    func showLoadingUI() {
        // ...
    }

    func hideLoadingUI() {
        //NotificationCenter
//            .default
//            .removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        // ...
    }
    
}
