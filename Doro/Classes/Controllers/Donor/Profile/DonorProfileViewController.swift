//
//  DonorProfileViewController.swift
//  Doro
//
//  Created by a on 07/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import ObjectMapper
import KeychainSwift


class DonorProfileViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var donorProfileImage: UIImageView!
    @IBOutlet weak var donorNameLabel: UILabel!
    @IBOutlet weak var donorOccupationLabel: UILabel!
    @IBOutlet weak var uiSwitch: UISwitch!
    @IBOutlet weak var deleteCredentialsView: UIView!
    
    
    //MARK: - Variables
    var window : UIWindow?
    var donorNotificationVC = DonorNotificationsViewController()
    var donorProfileSettingVC = DonorProfileSettingViewController()
    var donorselectPaymentVC = SelectPaymentViewController()
    var loginVC = LoginViewController()
    let keychain = KeychainSwift()
    
    
    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        dataSource()
        deleteCredentialsView.isHidden = true
        
        if let credentails = keychain.get("credentials") {
            let cred = credentails.components(separatedBy: ",")
            
            if cred[0] == DataManager.shared.getUser()?.user.email {
                deleteCredentialsView.isHidden = false

            }
        }
    }
    
    
    //MARK: - Setup View
    func setupView() {
        self.navigationController?.navigationBar.isHidden = true
        donorProfileImage.cornerRadius = donorProfileImage.frame.width / 2
    }
    
    
    //MARK: - IBActions
    @IBAction func notificationTapped(_ sender: Any) {
        donorNotificationVC = DonorNotificationsViewController()
        self.navigationController?.pushViewController(donorNotificationVC, animated: true)
    }
    
    @IBAction func profileSettingsTapped(_ sender: Any) {
        donorProfileSettingVC = DonorProfileSettingViewController()
        self.navigationController?.pushViewController(donorProfileSettingVC, animated: true)
    }
    
    @IBAction func defaultPaymentPressed(_ sender: Any) {
        donorselectPaymentVC = SelectPaymentViewController()
        donorselectPaymentVC.isChangePaymentGateway = true
        self.navigationController?.pushViewController(donorselectPaymentVC, animated: true)
    }
    
    @IBAction func paymentMethodTapped(_ sender: Any) {
        donorselectPaymentVC = SelectPaymentViewController()
        self.navigationController?.pushViewController(donorselectPaymentVC, animated: true)
    }
    
    @IBAction func deletePressed(_ sender: Any) {
        showDeleteConfirmation ()
    }
    
    @IBAction func logoutTapped(_ sender: Any) {
        DataManager.shared.deleteUser()
        Utility.loginRootViewController()
    }
    
    
    //MARK: Datasouce
    func dataSource() {
        
        Utility.showLoading()
        UserProfileData.getUserProfile ({[weak self] (result, error, status) in
                
            if error == nil {
                self?.donorNameLabel.text = result?.fullName
                self?.donorOccupationLabel.text = result?.occupation
                let imageUrl = result?.imageUrl
                let trimmedImageUrl = imageUrl?.replacingOccurrences(of: "\\s+$", with: "", options: .regularExpression)
                
                if let url = URL(string: APIRoutes.baseUrl + APIRoutes.imageBaseUrl + trimmedImageUrl!) {
                    self?.donorProfileImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "profilePlaceholder"))
                }
                
            } else {
                self?.showAlert(title: "", message: "Something went wrong")
            }
        })
    }
    
    func showDeleteConfirmation () {
        
        if let _ = keychain.get("credentials") {
            let alert = UIAlertController(title: "Delete FaceID/TouchID", message: "Please confirm to delete local authentication.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: {_ in
                self.keychain.delete("credentials")
                DataManager.shared.deleteFinger()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler:nil))
            self.present(alert, animated: true, completion:nil)
            
        } else {
            self.showAlert(title: "", message: "You did not configure FaceID/TouchID.")
        }
    }
}
