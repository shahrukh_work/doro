//
//  Constants.swift
//  Doro
//
//  Created by a on 04/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit

var kApplicationWindow = Utility.getAppDelegate()!.window
var skipButtonTapped = false
var retryButtonPressed = false
var uiButtonColor = false
var backButtonTapped = false
var backOnRetryTapped = false
var notificationButton = false
var tapValidate = false
var tapToCancel = false

var apiRequestObject: [String: String] = [:]

enum PaymentMethod {
    case paypal
    case venmo
    case creditCard
}


enum UserType: String {
    case donor = "1"
    case donee = "0"
}


struct APIRoutes {
    static var baseUrl = "https://doro-backend.codesorbit.com"
    static var imageBaseUrl = "/api/shared/user/userImage"
    static var login = "/api/auth/login"
    static var getUserProfile = "/api/shared/user/userProfile"
    static var createUser = "/api/shared/user/create"
    static var updateUser = "/api/shared/user/update"
    static var forgotPassword = "/api/shared/user/forgetPassword"
    static var widthdrawFromDonee = "/api/payments/paypal/donee/withdrawRequest"
    static var getDoneeUserTransactions = "/api/payments/paypal/donee/transactions"
    static var getDonorUserTransactions = "/api/payments/paypal/donor/transactions"
    static var getDoneeNotifications = "/api/shared/user/donee/getNotifications"
    static var donorNotification = "/api/shared/user/donor/getNotifications"
    static var donorToAdminTransfer = "/api/payments/paypal/donor/transfer"
    static var doneeReceivedAndWithdrawnAmount = "/api/payments/paypal/donee/wallet"
    static var donorReceivedAndWithdrawnAmount = "/api/payments/paypal/donor/wallet"
    static var ephemeralKeys = "/api/payments/stripe/getEphemeralKeys"
    static var updatePaypalEmail = "/api/shared/user/update/mobile/payment/paypal"
    static var stripeDonorTransaction = "/api/payments/stripe/update/transactions"
    static var createCharge = "/api/payments/stripe/createPaymentIntent"
    static let defaultPaymentGateway = "/api/shared/user/update/payment/method"
    static let createPaymentGateway = "/api/shared/user/update/paymentEmail"
    static let changeStripeEmail = ""
    static let updateStatus = "/api/payments/update/status"
    static let checkPendingPayment = "/api/payments/validate/pendingPayment"
}
