//
//  MyAPIClient.swift
//  Doro
//
//  Created by Mapple Technologies on 09/11/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import Foundation
import Stripe

class MyAPIClient: NSObject, STPCustomerEphemeralKeyProvider {
    
  
    enum APIError: Error {
        case unknown

        var localizedDescription: String {
            switch self {
            case .unknown:
                return "Unknown error"
            }
        }
    }

    static let sharedClient = MyAPIClient()
    var baseURLString: String? = APIRoutes.baseUrl
    
    var baseURL: URL {
        if let urlString = self.baseURLString, let url = URL(string: urlString) {
            return url
        } else {
            fatalError()
        }
    }
    
    func completeCharge(doneeId: Int, donorId: Int, _ result: STPPaymentResult, amount: String, viewController: UIViewController,_ shippingAddress: STPAddress?, shippingMethod: PKShippingMethod?, completion: @escaping STPPaymentStatusBlock) {

        Utility.showLoading()
        APIClient.shared.createCharge(params: ["amount": amount, "customer": UserData.shared.user.stripeCustId]) { (data, error, status) in
            
            if error == nil {
                guard let paymentIntentClientSecret = data as? String else {
                    return;
                }
                // Collect card details
                let paymentIntentParams = STPPaymentIntentParams(clientSecret: paymentIntentClientSecret)
                paymentIntentParams.paymentMethodId = result.paymentMethod?.stripeId
                
                // Submit the payment
                let paymentHandler = STPPaymentHandler.shared()
                paymentHandler.confirmPayment(withParams: paymentIntentParams, authenticationContext: viewController as! STPAuthenticationContext) { (status, paymentIntent, error) in
                    Utility.hideLoading()

                    switch (status) {
                    case .failed:
                        print("not working")
                        viewController.showAlert(title: "", message: error?.localizedDescription ?? "")
                        break
                    case .canceled:
                        viewController.showAlert(title: "", message: error?.localizedDescription ?? "")
                        break
                    case .succeeded:
                        print("working")
                        self.updatePaymentToServer(donor: donorId, donee: doneeId, amount: "\(amount)", viewController: viewController)
                        break
                    @unknown default:
                        fatalError()
                        break
                    }
                }
            }
        }
    }

    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
        let url = self.baseURL.appendingPathComponent(APIRoutes.ephemeralKeys)
        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
        urlComponents.queryItems = [URLQueryItem(name: "customer_id", value: DataManager.shared.getUser()?.user.stripeCustId)]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        
        let task = URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
            
            guard let response = response as? HTTPURLResponse,
                response.statusCode == 200,
                let data = data,
                let json = ((try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]) as [String : Any]??) else {
                completion(nil, error)
                return
            }
            completion(json!["data"] as! [AnyHashable : Any], nil)
        })
        task.resume()
    }
    
    func updatePaymentToServer (donor: Int, donee: Int, amount: String, viewController: UIViewController) {
 
        APIClient.shared.updateDonorTransactions(donorId: UserData.shared.user.id , doneeId: donee, amount: Int(amount)!) { (result, error, status) in
            Utility.hideLoading()
            if error == nil {
                let vc = viewController as? DonorTransferAmountViewController
                vc?.showSuccesVC()

            } else {
                viewController.showAlert(title:"", message: "Error create updating but transaction has been made.")
            }
        }
    }
}


