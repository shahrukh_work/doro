
import Foundation
import ObjectMapper

typealias GetTransactionCompletionHandler = (_ data: Transactions?, _ error: Error?, _ status: Int?) -> Void

typealias tArray = [Transaction]

class Transactions : Mappable {
    var error = false
    var message  = ""
    var data = [Transaction]() //[Transaction]() //Mapper<Transaction>().map(JSON: [:])!
    var errors = [""]
    
    required init?(map: Map) { }
    
    func mapping(map: Map) {
        
        error   <- map["error"]
        message <- map["message"]
        data    <- map["result"]
        errors  <- map["errors"]
    }
    
    class func getTransactions(userType: UserType = .donee, offset: Int,_ completion: @escaping GetTransactionCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getUserTransactions(userType: userType, offset: offset) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                
                if let data = Mapper<Transactions>().map(JSON: result as! [String:Any]) {
                    completion(data, nil, 200)
                }
                
            } else {
                completion(nil, nil, 201)
            }
        }
        
    }
}
