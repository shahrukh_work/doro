

import Foundation
import ObjectMapper

typealias DonorToAdminCompletionHandler = (_ result: DonorToAdmin?,_ error: Error?,_ status: Int?) -> ()

class DonorToAdmin : Mappable {
	var error : Bool?
	var message : String?
	var data : DonorData?
	var errors : [String]?

    required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

    class func paymentDonorToAdmin (donorId: Int, doneeId: String, amount: Double, password: String ,_ completion:  @escaping DonorToAdminCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.sendAmountFromDonorToAdmin(donorId: donorId, doneeId: doneeId, amount: amount, password: password) { (result, error, status) in
            Utility.hideLoading()

            if error == nil {
            
                let json = ["data": result]
                if let data = Mapper<DonorToAdmin>().map(JSON: json as! [String: Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, error, 401)
                }
            } else {
                completion(nil, error, 404)
            }
        }
    }
}
