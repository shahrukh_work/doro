

import Foundation
import ObjectMapper

struct Payout_item : Mappable {
	var recipient_type : String?
	var amount : Amount?
	var note : String?
	var receiver : String?
	var sender_item_id : String?
	var recipient_wallet : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		recipient_type <- map["recipient_type"]
		amount <- map["amount"]
		note <- map["note"]
		receiver <- map["receiver"]
		sender_item_id <- map["sender_item_id"]
		recipient_wallet <- map["recipient_wallet"]
	}

}
