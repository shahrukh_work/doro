

import Foundation
import ObjectMapper

struct Sender_batch_header : Mappable {
	var sender_batch_id : String?
	var email_subject : String?
	var email_message : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		sender_batch_id <- map["sender_batch_id"]
		email_subject <- map["email_subject"]
		email_message <- map["email_message"]
	}

}
