
import Foundation
import ObjectMapper

struct Payout_item_fee : Mappable {
	var currency : String?
	var value : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		currency <- map["currency"]
		value <- map["value"]
	}

}
