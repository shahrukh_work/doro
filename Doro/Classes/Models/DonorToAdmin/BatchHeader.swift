
import Foundation
import ObjectMapper

class BatchHeader : Mappable {
	var payout_batch_id : String?
	var batch_status : String?
	var time_created : String?
	var time_completed : String?
	var time_closed : String?
	var sender_batch_header : Sender_batch_header?
	var funding_source : String?
	var amount : Amount?
	var fees : Fees?

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		payout_batch_id <- map["payout_batch_id"]
		batch_status <- map["batch_status"]
		time_created <- map["time_created"]
		time_completed <- map["time_completed"]
		time_closed <- map["time_closed"]
		sender_batch_header <- map["sender_batch_header"]
		funding_source <- map["funding_source"]
		amount <- map["amount"]
		fees <- map["fees"]
	}

}
