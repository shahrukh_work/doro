//
//  CheckPending.swift
//  Doro
//
//  Created by Mapple Technologies on 26/11/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CheckPendingCompletion = (_ data: CheckPending?, _ error: Error?, _ status: Int?) -> ()

class CheckPending: Mappable {
    var error = false
    var message = ""
    var data = [Payments]()
    var errors = [String]()
    
    required init?(map:Map){}
    
    func mapping(map: Map) {
        
        error <- map["error"]
        data <- map["data"]
        message <- map["message"]
        errors <- map["errors"]
    }
    
    class func loadData (email: String = DataManager.shared.getUser()?.user.email ?? "", _ completion: @escaping CheckPendingCompletion) {
        
        Utility.showLoading()
        APIClient.shared.checkPendingPayments(email: email) { (result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
                let json = ["data": result]
                if let data = Mapper<CheckPending>().map(JSON:  json as! [String : Any]) {
                    completion(data, nil,status)
                    return
                }
                completion(Mapper<CheckPending>().map(JSON: [:]), nil,status)
                
            } else {
                completion(nil, error,status)
            }
        }
    }
}

// MARK: - Datum
class Payments: Mappable {
    var id = -1
    var status = ""
    var createdAt = ""
    var userID = ""
    var email = ""
    var pendingAmount = 0.0
    var doneeID = -1

    required init?(map:Map){}
    
    func mapping(map: Map) {
        id <- map["id"]
        status <- map["status"]
        createdAt <- map["created_at"]
        userID <- map["user_id"]
        email <- map["email"]
        pendingAmount <- map["pending_amount"]
        doneeID <- map["donee_id"]
    }
}
