
import Foundation
import ObjectMapper

class DoneeNotification : Mappable {
	var id = -1
	var createdAt = ""
	var updatedAt = ""
	var doneeEmail = ""
	var amount = -1
	var doneeId = -1

	required init?(map: Map) { }

    func mapping(map: Map) {

		id          <- map["id"]
		createdAt   <- map["created_at"]
		updatedAt   <- map["updated_at"]
		doneeEmail  <- map["donee_email"]
		amount      <- map["amount"]
		doneeId     <- map["donee_id"]
	}
}
