

import Foundation
import ObjectMapper

typealias DoneeNotificationCompletionHandler = (_ result: DoneeNotifications?,_ error: Error?,_ status: Int?) -> ()


class DoneeNotifications : Mappable {
	var error = false
	var message = ""
	var data = [DoneeNotificationData]()
	var errors = [String]()
    var count = 0

	required init?(map: Map) {

	}

    func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["result"]
		errors <- map["errors"]
        count <- map["count"]
	}

    class func doneeNotifications (offset: Int = 0, _ completion:  @escaping DoneeNotificationCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getDoneeNotifications(offset: offset) {(result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
             //   DoneeNotification(map: <#Map#>)
                let json = ["data": result]
                if let data = Mapper<DoneeNotifications>().map(JSON: result as! [String: Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, error, 401)
                }
            } else {
                
            }
        }
    }
}
