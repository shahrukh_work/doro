
import Foundation
import ObjectMapper

typealias DonorNotificationCompletionHandler = (_ result: DonorNotifications?,_ error: Error?,_ status: Int?) -> ()

class DonorNotifications : Mappable {
	var error = false
	var message = ""
	var data = [DonorNotificationData]()
	var errors = [String]()
    var count = 0

	required init?(map: Map) {}

    func mapping(map: Map) {
		error <- map["error"]
		message <- map["message"]
		data <- map["result"]
		errors <- map["errors"]
        count  <- map["count"]
	}
    
    class func donorNotifications (offset: Int = 0, _ completion:  @escaping DonorNotificationCompletionHandler) {
        
        APIClient.shared.getDonorNotifications(offset: offset) {(result, error, status) in
            Utility.hideLoading()
            
            if error == nil {
            
                if let data = Mapper<DonorNotifications>().map(JSON: result as! [String: Any]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, error, 401)
                }
            } else {
                
            }
        }
    }
}
