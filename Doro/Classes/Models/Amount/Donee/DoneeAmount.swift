



import Foundation
import ObjectMapper

typealias DoneeAmountCompletionHandler = (_ result: DoneeAmount?,_ error: Error?,_ status: Int?) -> ()

struct DoneeAmount : Mappable {
	var error = false
	var message = ""
	var data = [DoneeAmountData]()
	var errors = [String]()

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

    static func getDoneeReceivedAndWithdrawnAmount (_ completion:  @escaping DoneeAmountCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getDoneeWithDrawnAndReceivedAmountMethod {(result, error, status) in
            Utility.hideLoading()
            
            let json = ["data": result]
            if error == nil {
                
                if let data = Mapper<DoneeAmount>().map(JSON: json as! [String: [Any]]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, error, 401)
                }
            } else {
                
            }
        }
    }
}
