

import Foundation
import ObjectMapper

typealias DonorAmountCompletionHandler = (_ result: DonorAmount?,_ error: Error?,_ status: Int?) -> ()

struct DonorAmount : Mappable {
	var error : Bool?
	var message : String?
	var data = [DonorAmountData]()
	var errors : [String]?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		error <- map["error"]
		message <- map["message"]
		data <- map["data"]
		errors <- map["errors"]
	}

    static func getDonorReceivedAndWithdrawnAmount (_ completion:  @escaping DonorAmountCompletionHandler) {
        Utility.showLoading()
        
        APIClient.shared.getDonorWithDrawnAndReceivedAmountMethod {(result, error, status) in
          //  Utility.hideLoading()
            
            if error == nil {
            
                let json = ["data": result]
                if let data = Mapper<DonorAmount>().map(JSON: json as! [String: [Any]]) {
                    completion(data, nil, 200)
                    
                } else {
                    completion(nil, error, 401)
                }
            } else {
                
            }
        }
    }
}
