


import Foundation
import ObjectMapper

struct DonorAmountData : Mappable {
	var id = -1
	var donorId = -1
	var donorEmail = ""
	var totalTransfer = -1

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		donorId <- map["donor_id"]
		donorEmail <- map["donor_email"]
		totalTransfer <- map["total_transfer"]
	}
}
