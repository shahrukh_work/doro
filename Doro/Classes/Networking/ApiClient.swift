
import UIKit
import Alamofire
import ObjectMapper
import Stripe


class Connectivity {
    
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

let APIClientDefaultTimeOut = 40.0

class APIClient: APIClientHandler {
    
    fileprivate var clientDateFormatter: DateFormatter
    var isConnectedToNetwork: Bool?
    
    static var shared: APIClient = {
        let baseURL = URL(fileURLWithPath: APIRoutes.baseUrl)
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = APIClientDefaultTimeOut
        
        let instance = APIClient(baseURL: baseURL, configuration: configuration)
        
        return instance
    }()
    
    // MARK: - init methods
    
    override init(baseURL: URL, configuration: URLSessionConfiguration, delegate: SessionDelegate = SessionDelegate(), serverTrustPolicyManager: ServerTrustPolicyManager? = nil) {
        clientDateFormatter = DateFormatter()
        
        super.init(baseURL: baseURL, configuration: configuration, delegate: delegate, serverTrustPolicyManager: serverTrustPolicyManager)
        
        //        clientDateFormatter.timeZone = NSTimeZone(name: "UTC")
        clientDateFormatter.dateFormat = "yyyy-MM-dd" // Change it to desired date format to be used in All Apis
    }
    
    
    // MARK: Helper methods
    
    func apiClientDateFormatter() -> DateFormatter {
        return clientDateFormatter.copy() as! DateFormatter
    }
    
    fileprivate func normalizeString(_ value: AnyObject?) -> String {
        return value == nil ? "" : value as! String
    }
    
    fileprivate func normalizeDate(_ date: Date?) -> String {
        return date == nil ? "" : clientDateFormatter.string(from: date!)
    }
    
    var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    func getUrlFromParam(apiUrl: String, params: [String: AnyObject]) -> String {
        var url = apiUrl + "?"
        
        for (key, value) in params {
            url = url + key + "=" + "\(value)&"
        }
        url.removeLast()
        return url
    }
    
    // MARK: - Onboarding
    func signInMethod(email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["email": email, "password": password] as [String:String]
        _ = sendRequest(APIRoutes.login , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    func createUser(params: [String: String],_ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(APIRoutes.createUser , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    func forgotPasswordMethod(email: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["email_id": email, "base_url": "https://doro.codesorbit.com/home"]
        _ = sendRequest(APIRoutes.forgotPassword , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    func updateUser(params: [String: String],_ completionBlock: @escaping APIClientCompletionHandler) {
        _ = sendRequest(APIRoutes.updateUser , parameters: params as [String : AnyObject],httpMethod: .put , headers: nil, completionBlock: completionBlock)
    }
    
    // MARK: - Get Total balance
    func getTotalBalance(_ completionBlock: @escaping APIClientCompletionHandler) {
        
    }
    
    func getUserProfile(doneeId: Int = -1,_ completionBlock: @escaping APIClientCompletionHandler) {
        
        if doneeId == -1 {
            let headers = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
            let params = [:] as [String:String]
            _ = sendRequest(APIRoutes.getUserProfile , parameters: params as [String : AnyObject],httpMethod: .get , headers: headers, completionBlock: completionBlock)
            
        } else {
            let headers = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
            let params = ["donee_id":doneeId] as [String:Any]
            _ = sendRequest(APIRoutes.getUserProfile , parameters: params as [String : AnyObject],httpMethod: .get , headers: headers, completionBlock: completionBlock)
        }
    }
    
    func updateUserProfile(image: UIImage, name: String, email: String, phoneNo: String, nationalId: String, ssn: String, occupation: String, type: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let headers = ["Authorization": "Bearer "+DataManager.shared.getUser()!.token]
        let params = ["image": image, "full_name": name, "email": email, "phone_number": phoneNo, "national_id": nationalId, "social_security_number": ssn, "occupation": occupation ,"type": type] as [String : Any]
        sendRequestUsingMultipart(APIRoutes.baseUrl+APIRoutes.updateUser, parameters: params as [String : AnyObject] , httpMethod: .put, headers: headers, completionBlock: completionBlock)
    }
    
    func getUserTransactions(userType: UserType = .donee, offset: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params: [String: Any] = ["offset": "\(offset)", "limit": "15"]
        let headers = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        var url = APIRoutes.getDoneeUserTransactions
        
        if userType == .donor {
            url =  APIRoutes.getDonorUserTransactions
        }
        _ = sendRequest(url, parameters: params as [String : AnyObject] , httpMethod: .get, headers: headers, completionBlock: completionBlock)
    }
    
    func getDoneeNotifications(offset: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params: [String: Any] = ["offset": "\(offset)", "limit": "15"]
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        _ = sendRequest(APIRoutes.getDoneeNotifications, parameters: params as! [String: AnyObject], httpMethod: .get, headers: header, completionBlock: completionBlock)
    }
    
    func getDonorNotifications(offset: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params: [String: Any] = ["offset": "\(offset)", "limit": "15"]
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        _ = sendRequest(APIRoutes.donorNotification, parameters: params as! [String: AnyObject] , httpMethod: .get, headers: header, completionBlock: completionBlock)
    }
    
    func withdrawFromDoneeMethod(doneeId: Int, amount: Double , _ completionBlock: @escaping APIClientCompletionHandler) {
        let headers = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["donee_id": doneeId, "amount": amount] as [String : Any]
        _ = sendRequest(APIRoutes.widthdrawFromDonee, parameters: params as [String: AnyObject] , httpMethod: .post, headers: headers, completionBlock: completionBlock)
    }
    
    func sendAmountFromDonorToAdmin(donorId: Int ,doneeId: String, amount: Double, password: String , _ completionBlock: @escaping APIClientCompletionHandler) {
        let headers = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["donor_id":donorId,"donee_id": doneeId, "amount": amount, "password": password] as [String : Any]
        
        _ = sendRequest(APIRoutes.donorToAdminTransfer, parameters: params as [String: AnyObject] , httpMethod: .post, headers: headers, completionBlock: completionBlock)
    }
    
    func getDoneeWithDrawnAndReceivedAmountMethod( _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        _ = sendRequest(APIRoutes.doneeReceivedAndWithdrawnAmount, parameters: nil, httpMethod: .get, headers: header, completionBlock: completionBlock)
    }
    
    func getDonorWithDrawnAndReceivedAmountMethod( _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        _ = sendRequest(APIRoutes.donorReceivedAndWithdrawnAmount, parameters: nil, httpMethod: .get, headers: header, completionBlock: completionBlock)
    }
    
    func updatePaypalEmail (oldPaypalEmail: String, newPaypalEmail: String, confirmNewPaypalEmail: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let headers = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["old_paypal_email": oldPaypalEmail, "new_paypal_email": newPaypalEmail, "confirm_new_email": confirmNewPaypalEmail] as [String : Any]
        
        _ = sendRequest(APIRoutes.updatePaypalEmail , parameters: params as [String: AnyObject] , httpMethod: .put, headers: headers, completionBlock: completionBlock)
    }
    
    func updateDonorTransactions(donorId: Int, doneeId: Int, amount: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["donee_id":doneeId, "donor_id": donorId, "amount": amount]
        _ = sendRequest(APIRoutes.stripeDonorTransaction, parameters: params as [String: AnyObject], httpMethod: .post, headers: header, completionBlock: completionBlock)
    }
    
    func createCharge(params: [String: Any], _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = params
        _ = sendRequest(APIRoutes.createCharge, parameters: params as [String: AnyObject], httpMethod: .post, headers: header, completionBlock: completionBlock)
    }
    
    func defaultPaymentGatewayMethod(paymentMethod: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["payment_method": paymentMethod]
        _ = sendRequest(APIRoutes.defaultPaymentGateway, parameters: params as [String: AnyObject], httpMethod: .put, headers: header, completionBlock: completionBlock)
    }
    
    func updateGatewayEmail(paymentMethod: String, paymentEmail: String,_ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["method": paymentMethod, "payment_email": paymentEmail]
        _ = sendRequest(APIRoutes.createPaymentGateway, parameters: params as [String: AnyObject], httpMethod: .put, headers: header, completionBlock: completionBlock)
    }
    
    func updateStripeEmail(customerId: String, newEmail: String, oldEmail: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["customer_id": customerId, "new_email": newEmail, "old_email": oldEmail]
        _ = sendRequest(APIRoutes.changeStripeEmail, parameters: params as [String: AnyObject], httpMethod: .put, headers: header, completionBlock: completionBlock)
    }
    
    func checkPendingPayments(email: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["donor_email": email]
        _ = sendRequest(APIRoutes.checkPendingPayment, parameters: params as [String: AnyObject], httpMethod: .post, headers: header, completionBlock: completionBlock)
    }
    
    func updateStatus(email: String, _ completionBlock: @escaping APIClientCompletionHandler) {
        let header = ["Authorization": "Bearer " + DataManager.shared.getUser()!.token]
        let params = ["donor_email": email, "status": true] as [String : Any]
        _ = sendRequest(APIRoutes.updateStatus, parameters: params as [String: AnyObject], httpMethod: .put, headers: header, completionBlock: completionBlock)
    }
    
    /*
    //MARK: - Get Cars
    func getAllCars(offset: Int, limit: Int, regNo: String,
                    makeYear: Int, carMake: String, carModel: String,
                    engineCapacity: String, color: String,
                    fuelType: String, carBadge: String,
                    transmission: String, minKlms: Int, maxKlms: Int,
                    maxPrice: Int, minPrice: Int, orderColumn: String, orderType: String,
                    _ completionBlock: @escaping APIClientCompletionHandler) {
        
        let params = ["offset" : offset, "limit" : limit, "reg_no" : regNo , "make_year" : makeYear, "car_make" : carMake, "car_model" : carModel, "engine_capacity" : engineCapacity , "color" : color ,"fuel_type" : fuelType, "car_badge" : carBadge , "transmission" : transmission, "car_km_min" : minKlms, "car_km_max" : maxKlms , "car_price_max" : maxPrice , "car_price_min" : minPrice , "orderColumn" : orderColumn , "orderType" : orderType ] as [String : AnyObject]
        //let params = ["offset" : offset, "limit" : limit] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getCars , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Cars
    func getAllCarsBySearch(offset: Int, limit: Int, searchKey: String,
                    _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["offset" : offset, "limit" : limit, "search" : searchKey] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getCars , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get CarDetails
    func getCarDetails(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["id" : id] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getCarDetails , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    func getNewsFeed (limit: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["limit" : limit] as [String : AnyObject]
        _ = sendRequest(APIRoutes.getNotifications , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Car Makes
    func getCarMakes(_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = [:] as [String : AnyObject]
        _ = sendRequest(APIRoutes.carMakes , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Car Models
    func getCarModels(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["id" : id] as [String : AnyObject]
        _ = sendRequest(APIRoutes.carModels , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get About Us
    func getAboutUs(_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = [:] as [String:String]
        _ = sendRequest(APIRoutes.aboutUs , parameters: params as [String : AnyObject],httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Contacted Users
    func getContactedUsers(offset: Int, limit: Int,_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["offset" : offset, "limit" : limit] as [String : AnyObject]
        _ = sendRequest(APIRoutes.contactedUsers , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Create Car Valuation
    func createCarValuation(name: String, email: String, phoneNo: String, regNo: String, carMake: String, carModel: String, engineCapacity: String, color: String, klms: Int, carBadge: String, fuelType: String, bodyType: String, transmission: String, makeYear: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["name": name, "email" : email, "phone" : phoneNo, "reg_no" : regNo, "car_make" : carMake, "car_model" : carModel, "engine_capacity" : engineCapacity, "color" : color, "km_driven" : klms, "car_badge" : carBadge, "fuel_type" : fuelType, "body_type" : bodyType, "transmission" : transmission, "make_year" : makeYear] as [String:AnyObject]
        _ = sendRequest(APIRoutes.carValuation , parameters: params ,httpMethod: .post , headers: nil, completionBlock: completionBlock)
    }
    
    //MARK: - Get Chat History
    func getChatHistory(_ completionBlock: @escaping APIClientCompletionHandler) {
        let params = ["limit" : 1000, "offset" : 0, "email" : UserDefaults.standard.string(forKey: "email")] as [String : AnyObject]
        _ = sendRequest(APIRoutes.chatHistory , parameters: params ,httpMethod: .get , headers: nil, completionBlock: completionBlock)
    }
    
    @discardableResult
    func getOnlineAdminMethod(_ completionBlock: @escaping APIClientCompletionHandler) -> Request {
        return sendRequest(APIRoutes.onlineAdmin, parameters: nil, httpMethod: .get, headers: nil, completionBlock: completionBlock)
    }
    
    */
    
    
    
//    func createCarValuation(name: String, email: String, phoneNo: String, regNo: String, carMake: String, carModel: String, engineCapacity: String, color: String, klms: Int, carBadge: String, fuelType: String, bodyType: String, transmission: String, makeYear: Int ,_ completionBlock: @escaping APIClientCompletionHandler) {
//        let params = ["name": name, "email" : email, "phone" : phoneNo, "reg_no" : regNo, "car_make" : carMake, "car_model" : carModel, "engine_capacity" : engineCapacity, "color" : color, "km_driven" : klms, "car_badge" : carBadge, "fuel_type" : fuelType, "body_type" : bodyType, "transmission" : transmission, "make_year" : makeYear] as [String:AnyObject]
//        sendRequestUsingMultipart(APIRoutes.carValuation , parameters: params as [String : AnyObject],httpMethod: .post , headers: nil, completionBlock: completionBlock)
//    }
//
//    func signInWebViewMethod(email: String , password: String, _ completionBlock: @escaping APIClientCompletionHandler) {
//        let params = ["user": email, "pass": password] as [String:String]
//        sendRequestUsingMultipart(APIRoutes.webViewBaseUrl + APIRoutes.signInWebView, parameters: params as [String : AnyObject] , httpMethod: .post, headers: nil, completionBlock: completionBlock)
//    }

    
    //MARK: - Add Food Screen
//    func getSingleFood(id: Int, _ completionBlock: @escaping APIClientCompletionHandler) {
//        let parameter = ["id": id,"with":"weights,properties"] as [String : AnyObject]
//        let headers = ["App":kAppName, "AppToken":kAppToken, "Authorization": "Bearer "+DataManager.shared.getAuthentication()!.accessToken]
//        _ = rawRequest(url: APIRoutes.baseUrl + APIRoutes.kGetFood+"/\(id)", method: .get, parameters: parameter, headers: headers, completionBlock: completionBlock)
//    }
    
    //
    //    @discardableResult
    //    func getTopChartEpisodes(_ completionBlock:@escaping APIClientCompletionHandler)->Request {
    //        let tokenString = "bearer "+User.shared.profileData!.token
    //        let params = [:] as [String:AnyObject]
    //        return sendRequest(APIRoutes.getTopChartEpisodes, parameters: params, headers:["Content-Type":  "application/json", "Authorization": tokenString], completionBlock: completionBlock)
    //    }
    
    
    
    
    //
    //extension UIImage {
    //    func rotate(radians: Float) -> UIImage? {
    //        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
    //        // Trim off the extremely small float value to prevent core graphics from rounding it up
    //        newSize.width = floor(newSize.width)
    //        newSize.height = floor(newSize.height)
    //
    //        UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
    //        let context = UIGraphicsGetCurrentContext()!
    //
    //        // Move origin to middle
    //        context.translateBy(x: newSize.width/2, y: newSize.height/2)
    //        // Rotate around middle
    //        context.rotate(by: CGFloat(radians))
    //        // Draw the image at its center
    //        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
    //
    //        let newImage = UIGraphicsGetImageFromCurrentImageContext()
    //        UIGraphicsEndImageContext()
    //
    //        return newImage
    //    }
}

