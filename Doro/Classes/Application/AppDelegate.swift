//
//  AppDelegate.swift
//  Doro
//
//  Created by a on 02/10/2020.
//  Copyright © 2020 codesrbit. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import ObjectMapper
import Firebase
import netfox
import FirebaseMessaging
import Stripe
import Braintree

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        NFX.sharedInstance().start()
        FirebaseApp.configure()
        BTAppSwitch.setReturnURLScheme("com.codesorbit.doro.payments")
        Stripe.setDefaultPublishableKey("pk_test_51HYsyMATtFGszbKC7jCmcgiYEkJSuoC0HdJwnnsawxth5fUx6UZhfPjqQiuy3sH9d4osEzTgJKQHKFbM9xxwiArp00HiGdyM98")

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 30
        registerForPushNotification()
        
        if let user = DataManager.shared.getUser() {
            UserData.shared = user
            
            if user.user.userType == "donor" {
                Utility.setDonorTabAsRootViewController()
                
            } else {
                Utility.setDoneeTabAsRootViewController()
            }
            
        } else {
            Utility.loginRootViewController()
        }
        return true
    }
    
    
    func registerForPushNotification() {
        UNUserNotificationCenter.current().delegate = self
        
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:  [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    
                    DispatchQueue.main.async {
                        UIApplication.shared.registerForRemoteNotifications()
                        
                    }
                }
            }
            
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }


    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        if url.scheme?.localizedCaseInsensitiveCompare("com.codesorbit.doro.payments") == .orderedSame {
           return BTAppSwitch.handleOpen(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String)
        
            
           // return Braintree.handleOpen(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String)
            }
            return false

    }

    // This method is where you handle URL opens if you are using univeral link URLs (eg "https://example.com/stripe_ios_callback")
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
//                let stripeHandled = StripeAPI.handleURLCallback(with: url)
//
//                if stripeHandled {
//                    return true
//                } else {
//                    // This was not a stripe url, do whatever url handling your app
//                    // normally does, if any.
//                }
            }

        }
        return false
    }
}


//MARK: - UNUserNotificationCenterDelegate

@available(iOS 10, *)
extension AppDelegate: MessagingDelegate {

    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        
//        UpdateToken.updateToken(token: fcmToken ?? "") { (result, error, status) in
//            
//            if error == nil {
//                print(result?.responseMessage ?? "")
//            }
//        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,willPresent notification: UNNotification,withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        print(notification.request.content.userInfo)
        if let aps = notification.request.content.userInfo["aps"] as? [String:Any], let alert = aps["alert"] as? [String:Any], let title = alert["title"] as? String {}
        completionHandler([[.alert, .sound]])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print("didReceive response")
        print(userInfo)
        completionHandler()
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        // 1. Convert device token to string
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }

        let token = tokenParts.joined()

        // 2. Print device token to use for PNs payloads

        print("Device Token: \(token)")
        Messaging.messaging().apnsToken = deviceToken
    }

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
    }

}

